import React, { useRef } from 'react'
import { Tile } from '../Tile/Tile'
import { ROOM_URL_CLONWERK, useCall } from './useCall'
// import Tile from './Tile';

export const App: React.FC = () => {
  const name = useRef('My very foo name ' + Math.random().toFixed(10)).current

  const {
    enableStartButton,
    startJoiningCall,
    startLeavingCall,
    callState,
    localUser,
  } = useCall(name)

  const onClickButton = () => {
    if (enableStartButton) {
      startJoiningCall(ROOM_URL_CLONWERK)
    } else {
      startLeavingCall()
    }
  }

  return (
    <div>
      <div>{name}</div>
      <div>State of the call: {callState}</div>
      <button onClick={onClickButton}>
        {' '}
        {enableStartButton ? 'Join the call' : 'End the call'}
      </button>

      <div style={{ position: 'relative' }}>
        <div
          style={{
            position: 'absolute',
            border: '1px solid silver',
            backgroundColor: '#e8e8e847',
            padding: '2rem',
          }}
        >
          <pre>{JSON.stringify(localUser, null, 2)}</pre>
        </div>

        <Tile
          videoTrackState={localUser?.tracks.video ?? null}
          audioTrackState={localUser?.tracks.audio ?? null}

          //disableCornerMessage={isScreenShare(id)}
        />
      </div>
    </div>
  )
}
