import { useCallback, useEffect, useState } from 'react'

import DailyIframe, {
  DailyCall,
  DailyEvent,
  DailyParticipant,
} from '@daily-co/daily-js'
import { logCallState } from '../../utils/logger'

const STATE_IDLE = 'STATE_IDLE'
const STATE_JOINING = 'STATE_JOINING'
const STATE_LEAVING = 'STATE_LEAVING'
const STATE_ERROR = 'STATE_ERROR'
// const STATE_CREATING = 'STATE_CREATING'
// const STATE_JOINED = 'STATE_JOINED'

export const ROOM_URL_CLONWERK = 'https://clonwerk.daily.co/01test'

const events: DailyEvent[] = [
  'participant-joined',
  'participant-updated',
  'participant-left',
]

export const useCall = (username: string) => {
  const [callState, setAppState] = useState(STATE_IDLE)
  const [callObject, setCallObject] = useState<null | DailyCall>(null)
  const [localUser, setLocalUser] = useState<null | DailyParticipant>(null)

  /**
   * Starts joining an existing call.
   *
   * NOTE: In this demo we show how to completely clean up a call with destroy(),
   * which requires creating a new call object before you can join() again.
   * This isn't strictly necessary, but is good practice when you know you'll
   * be done with the call object for a while and you're no longer listening to its
   * events.
   */
  const startJoiningCall = useCallback(
    (url: string) => {
      const newCallObject = DailyIframe.createCallObject()
      setCallObject(newCallObject)
      logCallState('appState JOINING')
      setAppState(STATE_JOINING)
      newCallObject.join({ url })
      const aaa = newCallObject.setUserName(username)
      aaa.then((r) => console.log).catch((e) => console.error)
    },
    [username],
  )

  /**
   * Starts leaving the current call.
   */
  const startLeavingCall = useCallback(() => {
    if (!callObject) return
    // If we're in the error state, we've already "left", so just clean up
    if (callState === STATE_ERROR) {
      logCallState('appState ERROR')
      callObject.destroy().then(() => {
        setCallObject(null)
        setAppState(STATE_IDLE)
        setLocalUser(null)
      })
    } else {
      logCallState('appState LEAVING')
      setAppState(STATE_LEAVING)
      setLocalUser(null)
      callObject.leave()
    }
  }, [callObject, callState])

  useEffect(() => {
    if (!callObject) {
      return
    }

    function handleNewParticipantsState(event?: DailyEvent) {
      const participants = callObject!.participants()
      setLocalUser(participants.local)
    }

    handleNewParticipantsState()

    for (const event of events) {
      callObject.on(event, handleNewParticipantsState)
    }

    return function cleanup() {
      setLocalUser(null)

      for (const event of events) {
        callObject.off(event, handleNewParticipantsState)
      }

      callObject.leave().then(() => {
        callObject.destroy()
      })
    }
  }, [callObject])

  /**
   * Only enable the start button if we're in an idle state (i.e. not creating,
   * joining, etc.).
   *
   * !!!
   * IMPORTANT: only one call object is meant to be used at a time. Creating a
   * new call object with DailyIframe.createCallObject() *before* your previous
   * callObject.destroy() completely finishes can result in unexpected behavior.
   * Disabling the start button until then avoids that scenario.
   * !!!
   */
  const enableStartButton =
    callState === STATE_IDLE || callState === STATE_LEAVING

  return {
    enableStartButton,
    startJoiningCall,
    startLeavingCall,
    callState,
    localUser,
  }
}
