import { DailyTrackState } from '@daily-co/daily-js'
import React, { useEffect, useMemo, useRef } from 'react'
import { getTrackUnavailableMessage } from './getTrackUnavailableMessage'

export const Tile: React.FC<{
  videoTrackState: null | DailyTrackState
  audioTrackState: null | DailyTrackState
}> = ({ videoTrackState, audioTrackState }) => {
  const videoEl = useRef<null | HTMLVideoElement>(null)
  const audioEl = useRef<null | HTMLVideoElement>(null)

  const videoTrack = useMemo(() => {
    return videoTrackState && videoTrackState.state === 'playable'
      ? videoTrackState.track
      : null
  }, [videoTrackState])

  const audioTrack = useMemo(() => {
    return audioTrackState && audioTrackState.state === 'playable'
      ? audioTrackState.track
      : null
  }, [audioTrackState])

  const videoUnavailableMessage = useMemo(() => {
    return getTrackUnavailableMessage('video', videoTrackState)
  }, [videoTrackState])

  const audioUnavailableMessage = useMemo(() => {
    return getTrackUnavailableMessage('audio', audioTrackState)
  }, [audioTrackState])

  /**
   * When video track changes, update video srcObject
   */
  useEffect(() => {
    videoEl.current &&
      videoTrack &&
      (videoEl.current.srcObject = new MediaStream([videoTrack]))
  }, [videoTrack])

  /**
   * When audio track changes, update audio srcObject
   */
  useEffect(() => {
    audioEl.current &&
      audioTrack &&
      (audioEl.current.srcObject = new MediaStream([audioTrack]))
  }, [audioTrack])

  function getVideoComponent() {
    return videoTrack && <video autoPlay muted playsInline ref={videoEl} />
  }

  function getAudioComponent() {
    return audioTrack && <audio autoPlay playsInline ref={audioEl} />
  }

  function getOverlayComponent() {
    // Show overlay when video is unavailable. Audio may be unavailable too.
    return (
      videoUnavailableMessage && (
        <p className="overlay">
          {videoUnavailableMessage}
          {audioUnavailableMessage && (
            <>
              <br />
              {audioUnavailableMessage}
            </>
          )}
        </p>
      )
    )
  }

  function getCornerMessageComponent() {
    // Show corner message when only audio is unavailable.
    return (
      audioUnavailableMessage &&
      !videoUnavailableMessage && (
        <p className="corner">{audioUnavailableMessage}</p>
      )
    )
  }

  return (
    <div style={{ pointerEvents: 'none' }}>
      <div className="background" />
      {getOverlayComponent()}
      {getVideoComponent()}
      {getAudioComponent()}
      {getCornerMessageComponent()}
    </div>
  )
}
