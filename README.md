NOTE:

- appState puo' essere IDLE, CREATING, JOINING, JOINED, LEAVING, ERROR

  `callObject.leave()` termina la call ma non la distrugge (quindi si puo' rientrare senza ricreare callObject)
  `callObject.destroy()` rende l'oggetto callObject inutilizzabile e deve essere ricreato (in fase di errore si deve chiamare detroy)
